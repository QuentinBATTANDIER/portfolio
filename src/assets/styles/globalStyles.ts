import { createGlobalStyle } from 'styled-components';

export const GlobalStyles = createGlobalStyle`
  *, *::before, *::after {
    box-sizing: border-box;
  }
  * {
    margin: 0;
  }
  html, body {
    height: 100%;
    background-color: #242424;
    font-family: Inter, system-ui, Avenir, Helvetica, Arial, sans-serif;
  }
`;
