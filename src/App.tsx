import { GlobalStyles } from "./assets/styles/globalStyles";
import { Footer } from "./components/Footer/Footer.styles";
import Navbar from "./components/Navbar/Navbar";

function App() {
  return (
    <>
      <GlobalStyles />
      <div className="App">
        <Navbar />
        <Footer />
      </div>
    </>
  );
}

export default App;
