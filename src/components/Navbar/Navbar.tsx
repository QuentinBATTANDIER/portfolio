import React, { useState, useEffect, useRef } from "react";
import * as S from "./Navbar.styles";

const Navbar: React.FC = () => {
  const [isMobileNavOpen, setIsMobileNavOpen] = useState(false);

  const navItemsRef = useRef<HTMLDivElement>(null);
  const mobileNavItemsRef = useRef<HTMLDivElement>(null);
  const hamburgerRef = useRef<HTMLDivElement>(null);

  const handleHamburgerClick = () => {
    setIsMobileNavOpen(!isMobileNavOpen);
  };

  const handleDocumentClick = (event: MouseEvent) => {
    const target = event.target as HTMLElement;

    if (
      !navItemsRef.current?.contains(target) &&
      !mobileNavItemsRef.current?.contains(target) &&
      !hamburgerRef.current?.contains(target)
    ) {
      setIsMobileNavOpen(false);
    }
  };

  const openPDF = () => {
    const urlPdf = `${process.env.PUBLIC_URL}/cv.pdf`;
    window.open(urlPdf, "_blank");
  };

  useEffect(() => {
    document.addEventListener("click", handleDocumentClick);

    return () => {
      document.removeEventListener("click", handleDocumentClick);
    };
  }, []);

  return (
    <S.NavbarContainer>
      <S.Logo>{"<QB/>"}</S.Logo>
      <S.NavItems ref={navItemsRef}>
        <S.NavItem href="#about">À propos</S.NavItem>
        <S.NavItem href="#skills">Compétences</S.NavItem>
        <S.NavItem href="#projects">Projets</S.NavItem>
        <S.NavItem href="#contact">Contact</S.NavItem>
      </S.NavItems>
      <S.Resume className="ResumeNav" onClick={openPDF}>
        En savoir plus
      </S.Resume>
      <S.Hamburger ref={hamburgerRef} onClick={handleHamburgerClick}>
        <div />
        <div />
        <div />
      </S.Hamburger>
      {isMobileNavOpen && (
        <S.MobileNavItems ref={mobileNavItemsRef}>
          <S.NavItem href="#about" onClick={handleHamburgerClick}>
            À propos
          </S.NavItem>
          <S.NavItem href="#skills" onClick={handleHamburgerClick}>
            Compétences
          </S.NavItem>
          <S.NavItem href="#projects" onClick={handleHamburgerClick}>
            Projets
          </S.NavItem>
          <S.NavItem href="#contact" onClick={handleHamburgerClick}>
            Contact
          </S.NavItem>
          <S.MobileResume
            onClick={() => {
              handleHamburgerClick();
              openPDF();
            }}
          >
            En savoir plus
          </S.MobileResume>
        </S.MobileNavItems>
      )}
    </S.NavbarContainer>
  );
};

export default Navbar;
