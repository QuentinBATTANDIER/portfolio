import styled from 'styled-components';

export const NavbarContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: #333;
  padding: 0 1rem;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  z-index: 1000;

  @media (max-width: 768px) {
    justify-content: center;
  }
`;

export const NavItems = styled.div`
  display: flex;
  gap: 2rem;
  font-size: 20px;

  @media (max-width: 768px) {
    display: none;
  }
`;

export const MobileNavItems = styled(NavItems)`
  display: none;

  @media (max-width: 768px) {
    display: flex;
    text-align: center;
    flex-direction: column;
    gap: 1rem;
    position: absolute;
    top: 100%;
    left: 0;
    background-color: #333;
    padding: 1rem;
    width: 100%;
    div {
      height: 1px;
      background-color: grey;
    }
  }
`;

export const Logo = styled.div`
  font-size: 50px;
  color: white;
`;

export const NavItem = styled.a`
  color: white;
  text-decoration: none;

  &:hover {
    text-decoration: underline;
    color: white;
  }
`;

export const Resume = styled.button`
  background-color: #14B8A6;
  border-radius: 12px;
  padding: 10px;
  border: 4px solid #115E59;
  color: white;

  &:hover {
    border: 4px solid #115E59;
    cursor: pointer;
  }

  @media (max-width: 768px) {
    display: none;
  }
`;

export const MobileResume = styled(Resume)`
  @media (max-width: 768px) {
    display: block;
  }
`;

export const Hamburger = styled.div`
  display: none;
  flex-direction: column;
  gap: 4px;
  width: 20px;
  height: 20px;
  justify-content: space-between;

  &:hover {
    cursor: pointer;
  }

  div {
    height: 2px;
    background-color: white;
  }

  @media (max-width: 768px) {
    display: flex;
    position: absolute;
    right: 1rem;
  }
`;
